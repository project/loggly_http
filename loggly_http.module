<?php
/**
 * @file
 * Loggly HTTP module.
 */

use GuzzleHttp\Client,
    GuzzleHttp\Exception\RequestException;

define('LOGGLY_HTTP_LOG', 'http://logs-01.loggly.com/inputs/');
define('LOGGLY_HTTP_LOGGLY_TOKEN', variable_get('loggly_http_token'));

/**
 * Implements hook_menu().
 */
function loggly_http_menu() {
  $items = array();

  $items['admin/config/services/loggly-http-client'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => 'Loggly HTTP Client',
    'description' => 'Administer Loggly Client settings.',
    'access arguments' => array('administer loggly http client'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('loggly_http_admin'),
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function loggly_http_permission() {
  $permissions = array();

  $permissions['administer loggly http client'] = array(
    'title' => t('Administer Loggly HTTP client'),
  );

  return $permissions;
}

/**
 * Loggly HTTP POST API Configuration Form.
 */
function loggly_http_admin($form, &$form_state) {
  $form['loggly_http_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Loggly HTTP API Token'),
    '#description' => t('TOKEN: your customer token from the source setup page'),
    '#default_value' => variable_get('loggly_http_token', NULL),
  );

  $form['loggly_http_severity_level'] = array(
    '#type' => 'select',
    '#title' => t('Watchdog Severity'),
    '#options' => loggly_http_severity_levels(),
    '#default_value' => variable_get('loggly_http_severity_level', WATCHDOG_ERROR),
    '#description' => t('The minimum severity level to be reached before an event is pushed to Loggly.'),
  );

  $form['loggly_http_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Loggly HTTP API'),
    '#description' => t('Enable Loggly HTTP POST'),
    '#default_value' => variable_get('loggly_http_enabled'),
  );

  return system_settings_form($form);
}

/**
 * Returns an array of watchdog severity levels.
 *
 * @return array
 *   Array of Watchdog severity levels.
 */
function loggly_http_severity_levels() {
  return array(
    WATCHDOG_EMERGENCY => t('Emergency'),
    WATCHDOG_ALERT => t('Alert'),
    WATCHDOG_CRITICAL => t('Critical'),
    WATCHDOG_ERROR => t('Error'),
    WATCHDOG_WARNING => t('Warning'),
    WATCHDOG_NOTICE => t('Notice'),
    WATCHDOG_INFO => t('Info'),
    WATCHDOG_DEBUG => t('Debug'),
  );
}

/**
 * Implements hook_watchdog().
 *
 * HTTP POST JSON Object as a single event to Loggly.
 *
 * These events can be searched in Loggly by "tag:http".
 */
function loggly_http_watchdog(array $log_entry) {
  if (variable_get('loggly_http_enabled') && $log_entry['severity'] <= variable_get('loggly_http_severity_level')) {
    global $base_url;
    $message = array(
      'base_url' => $base_url,
      'timestamp' => $log_entry['timestamp'],
      'type' => $log_entry['type'],
      'ip' => $log_entry['ip'],
      'request_uri' => $log_entry['request_uri'],
      'referer' => $log_entry['referer'],
      'uid' => $log_entry['uid'],
      'link' => strip_tags($log_entry['link']),
      'uuid' => loggly_http_uuid_generate(),
      'message' => strip_tags(!isset($log_entry['variables']) ? $log_entry['message'] : strtr($log_entry['message'], $log_entry['variables'])),
    );
    $api = new Client();
    $loggly_endpoint = LOGGLY_HTTP_LOG . LOGGLY_HTTP_LOGGLY_TOKEN . "/tag/http/";
    $type = 'application/x-www-form-urlencoded';
    $message = drupal_json_encode($message);
    $request = $api->createRequest('POST', $loggly_endpoint, array(
      'headers' => array(
        'Accept' => 'text/plain',
        'Content-Type' => $type,
      ),
    'body' => $message)
    );
    try {
      $api->send($request);
    }
    catch (RequestException $e) {
      if ($e->hasResponse()) {
        watchdog('loggly_http', $e->getResponse(), WATCHDOG_ERROR);
      }
    }
  }
}

/**
 * Generates a UUID v4 using PHP code.
 *
 * Based on code from @see http://php.net/uniqid#65879.
 */
function loggly_http_uuid_generate() {
  // The field names refer to RFC 4122 section 4.1.2.
  return sprintf('%04x%04x-%04x-4%03x-%04x-%04x%04x%04x',
    // 32 bits for "time_low".
    mt_rand(0, 65535), mt_rand(0, 65535),
    // 16 bits for "time_mid".
    mt_rand(0, 65535),
    // 12 bits after the 0100 of (version) 4 for "time_hi_and_version".
    mt_rand(0, 4095),
    bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '10', 0, 2)),
    // 8 bits, the last two of which (positions 6 and 7) are 01, for "clk_seq_hi_res"
    // (hence, the 2nd hex digit after the 3rd hyphen can only be 1, 5, 9 or d)
    // 8 bits for "clk_seq_low" 48 bits for "node".
    mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)
  );
}
